package com.db.springBootSample.controller;

import com.db.springBootSample.Employee;
import com.db.springBootSample.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class HomeController {

	@Autowired
	private EmployeeRepository employeeRepository;

	@RequestMapping("/health")
	public String healthCheck() {
		return "UP";
	}

	@RequestMapping("/")
	public String home(@RequestParam(name="name", required=false, defaultValue="john") String name) {
		return "Welcome "+name+". This is a sample spring boot project";
	}

	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeRepository.findAll();
	}

}
