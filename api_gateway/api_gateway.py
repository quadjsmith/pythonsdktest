from aws_cdk import (
    core,
    aws_iam as _iam,
    aws_lambda as _lambda,
    aws_apigateway as _api)

swagger_json = {
    "swagger": "2.0",
    "info": {
        "title": "Player API",
        "description": "A simple API for Player resources",
        "version": "1.0.1",
        "contact": {
            "name": "John Smith",
            "url": "",
            "email": "johnsmith@quadyster.com"
        },
        "license": {
            "name": "Apache 2.0",
            "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
        },
        "termsOfService": ""
    },
    "paths": {
        "/player/{id}": {
            "get": {
                "description": "Returns a Player resource",
                "produces": [
                    "application/json"
                ],
                "responses": {
                    "200": {
                        "description": "A player resource.",
                        "schema": {
                            "$ref": "#/definitions/Player"
                        }
                    }
                },
                "x-amazon-apigateway-integration": {
                    "type": "http",
                    "responses": {
                        "default": {
                            "statusCode": "200"
                        }
                    },
                    "httpMethod": "GET",
                    "uri": "http://api.example.com"
                }
            },
            "parameters": [{
                "name": "id",
                "in": "path",
                "description": "Identifier of player to retreive",
                "required": True,
                "type": "string"
            }]
        }
    },
    "definitions": {
        "Player": {
            "type": "object",
            "properties": {
                "playerId": {
                    "type": "string"
                },
                "alias": {
                    "type": "string"
                },
                "displayName": {
                    "type": "string"
                },
                "profilePhotoUrl": {
                    "type": "string"
                }
            },
            "required": ["playerId", "alias"]
        }
    }
}

open_api = {
    "swagger": "2.0",
    "info": {
        "version": "2017-04-20T04:08:08Z",
        "title": "LambdaCalc"
    },
    "basePath": "/test",
    "schemes": [
        "https"
    ],
    "paths": {
        "/calc": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "operand2",
                        "in": "query",
                        "required": True,
                        "type": "string"
                    },
                    {
                        "name": "operator",
                        "in": "query",
                        "required": True,
                        "type": "string"
                    },
                    {
                        "name": "operand1",
                        "in": "query",
                        "required": True,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "200 response",
                        "schema": {
                            "$ref": "#/definitions/Result"
                        },
                        "headers": {
                            "operand_1": {
                                "type": "string"
                            },
                            "operand_2": {
                                "type": "string"
                            },
                            "operator": {
                                "type": "string"
                            }
                        }
                    }
                },
                "x-amazon-apigateway-request-validator": "Validate query string parameters and headers",
                "x-amazon-apigateway-integration": {
                    "credentials": "arn:aws:iam::833834609835:role/ECS-Role-Test",
                    "responses": {
                        "default": {
                            "statusCode": "200",
                            "responseParameters": {
                                "method.response.header.operator": "integration.response.body.op",
                                "method.response.header.operand_2": "integration.response.body.b",
                                "method.response.header.operand_1": "integration.response.body.a"
                            },
                            "responseTemplates": {
                                "application/json": "#set($res = $input.path('$'))\n{\n    \"result\": \"$res.a, $res.b, $res.op => $res.c\",\n  \"a\" : \"$res.a\",\n  \"b\" : \"$res.b\",\n  \"op\" : \"$res.op\",\n  \"c\" : \"$res.c\"\n}"
                            }
                        }
                    },
                    "uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:833834609835:function:Quad/invocations",
                    "passthroughBehavior": "when_no_match",
                    "httpMethod": "POST",
                    "requestTemplates": {
                        "application/json": "{\n    \"a\":  \"$input.params('operand1')\",\n    \"b\":  \"$input.params('operand2')\", \n    \"op\": \"$input.params('operator')\"   \n}"
                    },
                    "type": "aws"
                }
            },
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "in": "body",
                        "name": "Input",
                        "required": True,
                        "schema": {
                            "$ref": "#/definitions/Input"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "200 response",
                        "schema": {
                            "$ref": "#/definitions/Result"
                        }
                    }
                },
                "x-amazon-apigateway-request-validator": "Validate body",
                "x-amazon-apigateway-integration": {
                    "credentials": "arn:aws:iam::833834609835:role/ECS-Role-Test",
                    "responses": {
                        "default": {
                            "statusCode": "200",
                            "responseTemplates": {
                                "application/json": "#set($inputRoot = $input.path('$'))\n{\n  \"a\" : $inputRoot.a,\n  \"b\" : $inputRoot.b,\n  \"op\" : $inputRoot.op,\n  \"c\" : $inputRoot.c\n}"
                            }
                        }
                    },
                    "uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:833834609835:function:Quad/invocations",
                    "passthroughBehavior": "when_no_templates",
                    "httpMethod": "POST",
                    "type": "aws"
                }
            }
        },
        "/calc/{operand1}/{operand2}/{operator}": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "operand2",
                        "in": "path",
                        "required": True,
                        "type": "string"
                    },
                    {
                        "name": "operator",
                        "in": "path",
                        "required": True,
                        "type": "string"
                    },
                    {
                        "name": "operand1",
                        "in": "path",
                        "required": True,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "200 response",
                        "schema": {
                            "$ref": "#/definitions/Result"
                        }
                    }
                },
                "x-amazon-apigateway-integration": {
                    "credentials": "arn:aws:iam::833834609835:role/ECS-Role-Test",
                    "responses": {
                        "default": {
                            "statusCode": "200",
                            "responseTemplates": {
                                "application/json": "#set($inputRoot = $input.path('$'))\n{\n  \"input\" : {\n    \"a\" : $inputRoot.a,\n    \"b\" : $inputRoot.b,\n    \"op\" : \"$inputRoot.op\"\n  },\n  \"output\" : {\n    \"c\" : $inputRoot.c\n  }\n}"
                            }
                        }
                    },
                    "uri": "arn:aws:apigateway:us-east-1:lambda:path/2015-03-31/functions/arn:aws:lambda:us-east-1:833834609835:function:Quad/invocations",
                    "passthroughBehavior": "when_no_templates",
                    "httpMethod": "POST",
                    "requestTemplates": {
                        "application/json": "{\n   \"a\": \"$input.params('operand1')\",\n   \"b\": \"$input.params('operand2')\",\n   \"op\": #if($input.params('operator')=='%2F')\"/\"#{else}\"$input.params('operator')\"#end\n   \n}"
                    },
                    "contentHandling": "CONVERT_TO_TEXT",
                    "type": "aws"
                }
            }
        }
    },
    "definitions": {
        "Input": {
            "type": "object",
            "required": [
                "a",
                "b",
                "op"
            ],
            "properties": {
                "a": {
                    "type": "number"
                },
                "b": {
                    "type": "number"
                },
                "op": {
                    "type": "string",
                    "description": "binary op of ['+', 'add', '-', 'sub', '*', 'mul', '%2F', 'div']"
                }
            },
            "title": "Input"
        },
        "Output": {
            "type": "object",
            "properties": {
                "c": {
                    "type": "number"
                }
            },
            "title": "Output"
        },
        "Result": {
            "type": "object",
            "properties": {
                "input": {
                    "$ref": "#/definitions/Input"
                },
                "output": {
                    "$ref": "#/definitions/Output"
                }
            },
            "title": "Result"
        }
    },
    "x-amazon-apigateway-request-validators": {
        "Validate body": {
            "validateRequestParameters": False,
            "validateRequestBody": True
        },
        "Validate query string parameters and headers": {
            "validateRequestParameters": True,
            "validateRequestBody": False
        }
    }
}


class ApiGateway(core.Stack):

    def __init__(self, scope: core.Construct, stack_id: str, config_dict=None, config_file_path=None,
                 **kwargs) -> None:
        super().__init__(scope, stack_id, **kwargs)
        self.create_lambda()
        self.create_api_gateway()

    def create_lambda(self):
        with open("./api_gateway/lambda-handler.py", encoding="utf8") as fp:
            handler_code = fp.read()
        _lambda_for_api = _lambda.Function(self, 'Lambda', function_name='Quad',
                                           code=_lambda.InlineCode(handler_code),
                                           handler="index.main",
                                           role=_iam.Role.from_role_arn(self, 'LambdaRole',
                                                                        role_arn='arn:aws:iam::833834609835:role/service-role/Quad-role-vwe8r90c',
                                                                        mutable=False),
                                           runtime=_lambda.Runtime.PYTHON_3_7,
                                           timeout=core.Duration.seconds(300))

    def create_api_gateway(self):
        # api = _api.RestApi(self, 'RestApi')
        # quad = api.root.add_resource('quad')
        # quad.add_method('GET')
        # quad.add_method('POST')
        api = _api.CfnRestApi(self, 'RestAPI',
                              body=open_api,
                              name="RestAPIQuad",
                              fail_on_warnings=True)

