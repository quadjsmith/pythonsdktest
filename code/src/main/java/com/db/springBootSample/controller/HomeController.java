package com.db.springBootSample.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@RequestMapping("/health")
	public String healthCheck() {
		return "UP";
	}

	@RequestMapping("/")
	public String home(@RequestParam(name="name", required=false, defaultValue="John") String name) {
		return "Welcome "+name+". This is a sample spring boot project";
	}

}
