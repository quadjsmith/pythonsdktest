#!/usr/bin/env python3

from aws_cdk import core

from ecr.ecr_repo import EcrRepo
from ecs.web_app_ecs_fargate import WebAppEcsFargate

env_USA = core.Environment(account="833834609835", region="us-east-1")

app = core.App()
EcrRepo(app, "ECRStack", config_file_path='./configs/config_ecr.json',
                 env={'region': 'us-east-1', 'account': "833834609835"})
WebAppEcsFargate(app, "ECSStack", config_file_path='./configs/config_ecs.json',
                 env={'region': 'us-east-1', 'account': "833834609835"})

app.synth()
