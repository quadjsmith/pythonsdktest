import setuptools

with open("README.md") as fp:
    long_description = fp.read()

setuptools.setup(
    name="cdkworkshop",
    version="1.0.2",

    description="A sample CDK Python app",
    long_description=long_description,
    long_description_content_type="text/markdown",

    author="john",

    package_dir={"": "cdkworkshop"},
    packages=setuptools.find_packages(where="cdkworkshop"),

    install_requires=[
        "quadtalos==0.0.3"
    ],
    dependency_links=[
        'https://pkgs.dev.azure.com/mark0950/Cloud/_packaging/QuadTalos-Artifact%40Release/pypi/simple/'
    ],

    python_requires=">=3.6",

    classifiers=[
        "Development Status :: 4 - Beta",

        "Intended Audience :: Developers",

        "License :: OSI Approved :: Apache Software License",

        "Programming Language :: JavaScript",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",

        "Topic :: Software Development :: Code Generators",
        "Topic :: Utilities",

        "Typing :: Typed",
    ],
)
